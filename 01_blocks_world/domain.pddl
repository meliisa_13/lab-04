(define (domain blocksworld) 
  (:requirements :strips) 		; STRIPS required
  (:predicates (on ?x ?y) 		; block ?x is on block ?y
	       (ontable ?x)		; block ?x on the table
	       (clear ?x)		; no block is on block ?x
	       (handempty)		; robot’s arm is empty
	       (holding ?x)		; robot’s arm is holding ?x
	       )
  (:action pick-up			; action „pick up from the table”
	     :parameters (?block)
	     :precondition (and (clear ?block) (ontable ?block) (handempty))
	     :effect
	     (and (not (ontable ?block))
		  (not (clear ?block))
		  (not (handempty))
		  (holding ?block)))
  ; action „put on the table”
  ; action „put block A on block B” 
  ; action „take block A off block B” 
)