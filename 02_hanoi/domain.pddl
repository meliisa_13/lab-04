; generalize the blocks world domain to be a proper representation of a hanoi problem:
; 1) there is a fixed amount of places; places are objects just like blocks but they can't be picked up
; 2) blocks can be compared by size, block can be put only on the bigger one
; 3) use types to differentiate between blocks and places
; tip. Check the domain file for the basic hanoi problem your model should be able to solve
