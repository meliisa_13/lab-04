(define (problem four_blocks_three_places)
  (:domain hanoi)
  (:objects 
      b1 - block b2 - block b3 - block b4 - block 
      p1 - place p2 - place p3 - place
  )
  (:init 
    (handempty) (clear b4) (clear p2) (clear p3)
    (on b1 p1) (on b2 b1) (on b3 b2) (on b4 b3)
    (bigger b1 b2) (bigger b1 b3) (bigger b1 b4)
    (bigger b2 b3) (bigger b2 b4)
    (bigger b3 b4)
    (bigger p1 b1) (bigger p1 b2) (bigger p1 b3) (bigger p1 b4)
    (bigger p2 b1) (bigger p2 b2) (bigger p2 b3) (bigger p2 b4)
    (bigger p3 b1) (bigger p3 b2) (bigger p3 b3) (bigger p3 b4)
  )
  (:goal (and (on b1 p3) (on b2 b1) (on b3 b2) (on b4 b3)))
)